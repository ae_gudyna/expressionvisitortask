using System.Linq.Expressions;
using System;
using System.Collections.Generic;

namespace ExpressionVisitorTask
{
    using System.Linq;

    public class Modifier
    {
        public Expression Modify(Expression expression)
        {
            var incrementVisitor = new IncrementVisitor();
            return incrementVisitor.Visit(expression);
        }

        public Expression<T> Modify<T>(Expression<T> expression, Dictionary<string, object> pairs)
        {
            var replaceVisitor = new ReplaceParametersVisitor(pairs);
            var newExpression = replaceVisitor.Visit(expression);
            var incrementVisitor = new IncrementVisitor();
            
            return (Expression<T>)incrementVisitor.Visit(newExpression);
            
//            var incrementVisitor = new IncrementVisitor();
//            
//            return (Expression<T>)incrementVisitor.Visit(expression);
        }

        private class IncrementVisitor : ExpressionVisitor
        {
            private Dictionary<string, object> parameterPairs = new Dictionary<string, object>();

            protected override Expression VisitBinary(BinaryExpression node)
            {
                
                if (node.NodeType == ExpressionType.Add || node.NodeType == ExpressionType.Subtract)
                {
                    ParameterExpression param = null;
                    ConstantExpression constant = null;

                    if (node.Left.NodeType == ExpressionType.Constant)
                    {
                        constant = (ConstantExpression) node.Left;
                    }
                    else if (node.Left.NodeType == ExpressionType.Parameter)
                    {
                        param = (ParameterExpression) node.Left;
                    }
                    
                    if (node.Right.NodeType == ExpressionType.Constant)
                    {
                        constant = (ConstantExpression) node.Right;
                    }
                    else if (node.Right.NodeType == ExpressionType.Parameter)
                    {
                        param = (ParameterExpression) node.Right;
                    }

                    if (param != null && constant != null && constant.Type == typeof(int))
                        
                    {
                        if ((int) constant.Value == 1)
                        {
                            switch (node.NodeType)
                            {
                                case ExpressionType.Add:
                                    return Expression.Increment(param);
                                case ExpressionType.Subtract:
                                    return Expression.Decrement(param);
                            }
                        }
                        
                        if ((int) constant.Value == -1)
                        {
                            switch (node.NodeType)
                            {
                                case ExpressionType.Add:
                                    return Expression.Decrement(param);
                                case ExpressionType.Subtract:
                                    return Expression.Increment(param);
                            }
                        }
                    }
                }

                return base.VisitBinary(node);
            }
        }

        private class ReplaceParametersVisitor : ExpressionVisitor
        {
            private Dictionary<string, object> parameterPairs;

            public ReplaceParametersVisitor(Dictionary<string, object> pairs)
            :base()
            {
                parameterPairs = pairs;
            }
            
            protected override Expression VisitLambda<T>(Expression<T> node)
            {
                Expression body = this.Visit(node.Body);
                var parameters = node.Parameters.ToArray();
                
                return Expression.Lambda<T>(body, parameters);
            }
            
            protected override Expression VisitParameter(ParameterExpression node)
            {
                if (parameterPairs.TryGetValue(node.Name, out object value))
                {
                    if (node.Type == value.GetType()) return Expression.Constant(value);
                    throw new InvalidCastException($"{node.Type} is expected but {value.GetType()} is received");
                }

                return node;

            }
        }
    }
}