using System;
using System.Linq.Expressions;

namespace ExpressionVisitorTask
{
    using System.Xml;

    public class Reader : ExpressionVisitor
    {
        public void Visit<T>(Expression<T> node)
        {
            this.Visit(node.Body);
            Console.WriteLine();
        }
        protected override Expression VisitBinary(BinaryExpression node)
        {
            Console.Write("(");

            this.Visit(node.Left);

            switch (node.NodeType)
            {
                case ExpressionType.Add:
                    Console.Write(" + ");
                    break;

                case ExpressionType.Subtract:
                    Console.Write(" - ");
                    break;

                case ExpressionType.Divide:
                    Console.Write(" / ");
                    break;
            }

            this.Visit(node.Right);

            Console.Write(")");

            return node;
        }

        protected override Expression VisitUnary(UnaryExpression node)
        {
            if (node.NodeType == ExpressionType.Negate)
            {
                Console.Write("(-");
                this.Visit(node.Operand);
                Console.Write(")");
                return node;
            }

            this.Visit(node.Operand);
            switch (node.NodeType)
            {
                case ExpressionType.Increment:
                    Console.Write("++");
                    break;

                case ExpressionType.Decrement:
                    Console.Write("--");
                    break;
                
            }

            return node;
        }

        protected override Expression VisitConstant(ConstantExpression node)
        {
            string value;
            if (node.Value is int intValue)
            {
                value = (intValue < 0) ? $"({node.Value})" : node.Value.ToString();
            }
            else if (node.Value is double doubleValue)
            {
                value = (doubleValue < 0) ? $"({node.Value})" : node.Value.ToString();
            }
            else
            {
                value = node.Value.ToString();
            }
            Console.Write(value);
            return node;
        }

        protected override Expression VisitParameter(ParameterExpression node)
        {
            Console.Write(node.Name);
            return node;
        }
    }
}