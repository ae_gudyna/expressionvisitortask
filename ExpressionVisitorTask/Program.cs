﻿using System;
using System.Linq.Expressions;

namespace ExpressionVisitorTask
{
    using System.Collections.Generic;

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hi!");
            
            Expression<Func<int, int, int, int, int, int, double>> someExpr = 
                (a, b, c, x, y, z) => c - (x + y + (-b) + z*a + 1) / 3.0;
            var modifier = new Modifier();
            var reader = new Reader();
            reader.Visit(someExpr);
            var pairs = new Dictionary<string, object>();
            pairs.Add("y", -1);
            pairs.Add("b", -1);
            pairs.Add("a", -1);
            var changedExpr = modifier.Modify(someExpr, pairs);
            
            reader.Visit(changedExpr);
            Console.WriteLine(changedExpr.Compile().Invoke(0, 0, 0, 2, 0, 0));
            Console.ReadLine();
        }
    }
}